package com.example.app_nomina;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private EditText edtNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = findViewById(R.id.edtNombre);
        Button btnEntrar = findViewById(R.id.btnEntrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarNombreYAvanzar();
            }
        });
    }

    private void validarNombreYAvanzar() {
        String nombre = edtNombre.getText().toString().trim();

        if (nombre.isEmpty()) {
            Toast.makeText(this, "Por favor, ingresa el nombre del trabajador.", Toast.LENGTH_SHORT).show();
        } else {
            // Genera un folio único utilizando UUID (Universally Unique Identifier)
            String folio = UUID.randomUUID().toString();

            // Si el nombre no está vacío, pasamos a la siguiente actividad junto con el folio generado
            Intent intent = new Intent(this, calculoNomina.class);
            intent.putExtra("nombre_trabajador", nombre);
            intent.putExtra("folio", folio); // Enviamos el folio a la siguiente actividad.
            startActivity(intent);
        }
    }
}
