package com.example.app_nomina;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class calculoNomina extends AppCompatActivity {

    private TextView txtFolio, txtNombreTrabajador, txtSubtotal, txtImpuesto, txtTotalAPagar;
    private EditText edtHorasTrabajadas, edtHorasExtras, edtSubtotal, edtImpuesto, edtTotalAPagar;
    private RadioGroup radioGroupPuesto;
    private RadioButton radioAuxiliar, radioAlbanil, radioIngObra;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculo_nomina);

        txtFolio = findViewById(R.id.txtFolio);
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotalAPagar = findViewById(R.id.txtTotalAPagar);
        edtHorasTrabajadas = findViewById(R.id.edtHorasTrabajadas);
        edtHorasExtras = findViewById(R.id.edtHorasExtras);
        edtSubtotal = findViewById(R.id.edtSubtotal);
        edtImpuesto = findViewById(R.id.edtImpuesto);
        edtTotalAPagar = findViewById(R.id.edtTotalAPagar);
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto);
        radioAuxiliar = findViewById(R.id.radioAuxiliar);
        radioAlbanil = findViewById(R.id.radioAlbanil);
        radioIngObra = findViewById(R.id.radioIngObra);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Obtén el nombre del trabajador de los extras
        String nombreTrabajador = getIntent().getStringExtra("nombre_trabajador");

        // Generar un número de folio aleatorio de 10 cifras
        long folio = Math.round(Math.random() * 8999999999L) + 1000000000L;
        txtFolio.setText("FOLIO: " + folio);

        // Asigna el nombre al TextView correspondiente
        txtNombreTrabajador.setText(nombreTrabajador);

        // Definir el comportamiento del botón Calcular al hacer clic
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularNomina();
            }
        });

        // Definir el comportamiento del botón Limpiar al hacer clic
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        // Definir el comportamiento del botón Regresar al hacer clic
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Cierra la actividad actual y regresa a la actividad anterior
                finish();
            }
        });
    }

    // Método para calcular la nómina
    private void calcularNomina() {
        // Obtener las horas trabajadas y horas extras ingresadas por el usuario
        double horasTrabajadas = Double.parseDouble(edtHorasTrabajadas.getText().toString());
        double horasExtras = Double.parseDouble(edtHorasExtras.getText().toString());

        // Definir las tarifas y la tasa de impuesto según el puesto seleccionado
        double tarifaPorHora = 0.0;
        double tarifaHorasExtras = 0.0;

        if (radioAuxiliar.isChecked()) {
            tarifaPorHora = 50.0;
            tarifaHorasExtras = 100.0;
        } else if (radioAlbanil.isChecked()) {
            tarifaPorHora = 70.0;
            tarifaHorasExtras = 140.0;
        } else if (radioIngObra.isChecked()) {
            tarifaPorHora = 100.0;
            tarifaHorasExtras = 200.0;
        }

        // Calcular el subtotal, impuesto y total a pagar
        double subtotal = (horasTrabajadas * tarifaPorHora) + (horasExtras * tarifaHorasExtras);
        double impuesto = subtotal * 0.16;
        double totalAPagar = subtotal - impuesto;

        // Mostrar los resultados en los campos correspondientes
        edtSubtotal.setText(String.format("%.2f", subtotal));
        edtImpuesto.setText(String.format("%.2f", impuesto));
        edtTotalAPagar.setText(String.format("%.2f", totalAPagar));
    }

    // Método para limpiar los campos de entrada y resultados
    private void limpiarCampos() {
        txtFolio.setText("FOLIO: "); // Reiniciar el folio
        edtHorasTrabajadas.setText("");
        edtHorasExtras.setText("");
        edtSubtotal.setText("");
        edtImpuesto.setText("");
        edtTotalAPagar.setText("");
        radioGroupPuesto.clearCheck();
    }
}
